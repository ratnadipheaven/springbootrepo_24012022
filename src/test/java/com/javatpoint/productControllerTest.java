package com.javatpoint;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest
public class productControllerTest {

	@Autowired
    private MockMvc mockMvc;
	@MockBean
	private ProductService productService;
	
	 @Test
	    public void testProductContoller() throws Exception {
		  ArrayList<Product> products = new ArrayList<Product>(); //adding products to
		  products.add(new Product(100, "Mobile", "CLK98123", 9000.00, 6));
		  products.add(new Product(101, "Smart TV", "LGST09167", 60000.00, 3));
		  Mockito.when(productService.findAll()).thenReturn(products);
	      mockMvc.perform(get("/product")).andExpect(status().isOk());
	                
	    }
	 
	    @Test
	    public void testProductContollerforfetchProduct() throws Exception {
		 ArrayList<Product> products = new ArrayList<Product>();
		  products.add(new Product(100, "Mobile", "CLK98123", 9000.00, 6));
		  products.add(new Product(101, "Smart TV", "LGST09167", 60000.00, 3));
		  Mockito.when(productService.findAll()).thenReturn(products);
		  String body = (new ObjectMapper()).valueToTree(new ProductReq("5","test")).toString();
	        mockMvc.perform(post("/fetchProduct").contentType(MediaType.APPLICATION_JSON).content(body)).andExpect(status().isOk());
	                
	    }
}
