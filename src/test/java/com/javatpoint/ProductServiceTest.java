package com.javatpoint;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ProductServiceTest {
	
	@Autowired
	private ProductService productService;

	
	@Test
    public void testProductService() throws Exception {
	 
       List<Product> productList=productService.findAll();
 Assertions.assertNotNull(productList);                
    }
}
